<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class HomeController extends Controller
{
    public function index()
    {
        $sym_arr = ['@','#','^','*'];
        $name_arr = [2,3,4,5,6,7,8,9,10,'J','Q','K','A'];
        $cards_arr = $card_val_arr = [];
        foreach($name_arr as $vl){
            foreach($sym_arr as $sy){
                $cards_arr[]=$vl.$sy;
                $card_val_arr[$vl.$sy]['name']=$vl;
                $card_val_arr[$vl.$sy]['sy']=$sy;               
            }
        }
        $orig_caArr = $cards_arr;
        shuffle($cards_arr);
        $players_cr = [];
        $i = 1;
        foreach($cards_arr as $cr){
          $players_cr[$i]['det'][]=$cr;
          $players_cr[$i]['player_id']=$i;         
          $players_cr[$i]['name'][$card_val_arr[$cr]['name']] = ($players_cr[$i]['name'][$card_val_arr[$cr]['name']] ?? 0) + 1;
          if($i == 4){
            $i=0;
          }
          $i++;
        }
        $winner = self::checkWinningPlayer($players_cr);
       
        return view('home',['cards_arr'=>$orig_caArr, 'sf_cards_arr' => $cards_arr,'player_cards'=>$players_cr,'win'=>$winner]);
     }
     public function checkWinningLetter($playerCards,$ltr,$max_val){
        $win_list = [];
        $col_name = 'name.'.$ltr;   
        $player = collect($playerCards)->where($col_name, $max_val)->first();
      
        $win_list['player'] = $player['player_id'];

        $win_list['winning_ltr'] = array_filter($player['det'], function ($item) use ($ltr)
                                                {
                                                    if (stripos($item, $ltr) !== false) {
                                                        return true;
                                                    }
                                                    return false;
                                                });   
        return $win_list;       
     }
     public function checkWinningPlayer($playerCards){

        $check_series = ['A','K','Q','J'];
       
        foreach($check_series as $key => $ltr){
            $col_name = 'name.'.$ltr;
            $max_val = collect($playerCards)->max($col_name);
           
            switch($max_val){
                case 4 :
                case 3 :
                       return self::checkWinningLetter($playerCards,$ltr,$max_val);       
                break;
                case 2 :
                    $player = collect($playerCards)->where($col_name, $max_val)->toArray();

                    if(count($player) == 2){
                        $col_name_sy = $ltr.'*';
                        foreach($player as $pl){
                            $pl_win_with_symbol = collect($pl['det'])->contains($col_name_sy);
                            if($pl_win_with_symbol){
                                $win_list['player'] = $pl['player_id'];
                                $win_list['winning_ltr'] = array_filter($pl['det'], function ($item) use ($col_name_sy)
                                {
                                    if (stripos($item, $col_name_sy) !== false) {
                                        return true;
                                    }
                                    return false;
                                }); 
                                return $win_list;
                            }  
                        }                       
                    }
                    else{
                        return self::checkWinningLetter($playerCards,$ltr,$max_val);   
                    }                         
                break;
                case 1:
                       if($ltr == 'J'){
                        $win_list['player']="";
                        $win_list['winning_ltr']=[];
                        $win_list['msg'] = "Die Match";
                       }
                break;
            }
        }

     }

}
